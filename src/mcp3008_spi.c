#include <stdint.h>

#include "util.h"
#include "adc/mcp3008_spi.h"

/**
 * @brief      Obtain an analog-digital conversion of input to the MCP3008 ADC
 *
 * @param[in]  analog_input_channel  The analog input channel
 *
 * @return     digital conversion of analog ADC input
 */
int16_t mcp3008_get_adc_data(int analog_input_channel)
{
#if USE_WIRINGPI == 1

	return mcp3008_wiringpi(analog_input_channel);
#else

	return ERR_GENERAL_FAILURE;
#endif
} // End mcp3008_get_adc_data

#if USE_WIRINGPI == 1
#ifndef NDEBUG
#pragma message "MCP3008 WiringPi"
#endif

int16_t mcp3008_wiringpi(int analog_input_channel)
{
	int8_t adc_channel_config = MCP3008_SINGLE_CHANNEL_CONFIG;
	int8_t fd = 0;

	// Define 3 character buffer of members "1"
	unsigned char buffer[3] = { 1 };

	if ((fd = wiringPiSPISetup(MCP3008_SPI_CHANNEL, MCP3008_SPI_SPEED)) <
	    0) {
		print_error("wiringPiSPISetup", 0,
			    "Failed to initialize MCP3008 SPI");

		return ERR_GENERAL_FAILURE;
	}

	if ((analog_input_channel > ADC_CHANNEL_MAX) ||
	    (analog_input_channel < ADC_CHANNEL_MIN))
		return ERR_GENERAL_FAILURE;

	buffer[1] = (adc_channel_config + analog_input_channel) << 4;
	wiringPiSPIDataRW(MCP3008_SPI_CHANNEL, buffer, sizeof(buffer));

	close(fd);

	// Return last 10 bits
	return (int16_t)((buffer[1] & 3) << 8) + buffer[2];
} // End mcp3008_wiringpi
#endif
