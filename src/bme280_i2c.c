#include <stdint.h>
#include <math.h>

#include "util.h"
#include "multi/bme280_i2c.h"

/**
 * @brief      Gather relative humidity, temperature & air pressure metrics from the BME280 sensor
 *
 * @return     Metrics in a JSON-formatted string
 */
char *bme280_get_metrics(void)
{
#if USE_WIRINGPI == 1

	return bme280_wiringpi();
#else

	return "{\"humidity\": \"0\", \"temperature\": \"0\","
	       "\"pressure\": \"0\", \"altitude\": \"0\"}";
#endif
}

/**
 * @brief      Temperature compensation formula
 * @note      Returns temperature compensation in DegC, resolution is 0.01 DegC. Output value of
 *“5123”equals 51.23 DegC. t_fine carries fine temperature as global value
 *
 * @param      cal    Pointer to calibration variable
 * @param[in]  adc_T  The adc t
 *
 * @return     The temperature compensation value
 */
int32_t bme280_calculate_compensation(bme280_calibration_data *cal,
				      int32_t adc_T)
{
	int32_t var_1 = ((((adc_T >> 3) - ((int32_t)cal->dig_T1 << 1))) *
			 ((int32_t)cal->dig_T2)) >>
			11;

	int32_t var_2 = (((((adc_T >> 4) - ((int32_t)cal->dig_T1)) *
			   ((adc_T >> 4) - ((int32_t)cal->dig_T1))) >>
			  12) *
			 ((int32_t)cal->dig_T3)) >>
			14;

	return var_1 + var_2;
}

/**
 * @brief      Compensate the raw temperature metric
 *
 * @param[in]  t_fine  The t fine
 *
 * @return     Final temperature metric
 */
float bme280_compensate_temp(int32_t t_fine)
{
	return (float)((t_fine * 5 + 128) >> 8) / 100;
}

/**
 * @brief      Pressure compensation formula
 * @note      Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and
 *8 fractional bits). Output value of
 *             “24674867”represents 24674867/256 = 96386.2 Pa = 963.862 hPa
 *
 * @param[in]  adc_P   The adc p
 * @param      cal     Pointer to calibration variable
 * @param[in]  t_fine  The temperature compensation value
 *
 * @return     Finel air pressure metric
 */
float bme280_compensate_pressure(int32_t adc_P, bme280_calibration_data *cal,
				 int32_t t_fine)
{
	int64_t var_1;
	int64_t var_2;
	int64_t p;

	var_1 = ((int64_t)t_fine) - 128000;
	var_2 = var_1 * var_1 * (int64_t)cal->dig_P6;
	var_2 = var_2 + ((var_1 * (int64_t)cal->dig_P5) << 17);
	var_2 = var_2 + (((int64_t)cal->dig_P4) << 35);
	var_1 = ((var_1 * var_1 * (int64_t)cal->dig_P3) >> 8) +
		((var_1 * (int64_t)cal->dig_P2) << 12);
	var_1 = (((((int64_t)1) << 47) + var_1)) * ((int64_t)cal->dig_P1) >> 33;

	// Don't divide by zero
	if (!var_1)
		return var_1;

	p = 1048576 - adc_P;
	p = (((p << 31) - var_2) * 3125) / var_1;
	var_1 = (((int64_t)cal->dig_P9) * (p >> 13) * (p >> 13)) >> 25;
	var_2 = (((int64_t)cal->dig_P8) * p) >> 19;

	p = ((p + var_1 + var_2) >> 8) + (((int64_t)cal->dig_P7) << 4);

	return (float)p / 256;
}

/**
 * @brief      Humidity compensation formula
 * @note      Returns humidity in %RHas unsigned 32 bit integer in Q22.10format (22integer and
 *10fractional bits). Output value of
 *“47445”represents 47445/1024= 46.333%RH
 *
 * @param[in]  adc_H   The adc h
 * @param      cal     Pointer to calibration variable
 * @param[in]  t_fine  The temperature compensation value
 *
 * @return     The final relative humidity metric
 */
float bme280_compensate_humidity(int32_t adc_H, bme280_calibration_data *cal,
				 int32_t t_fine)
{
	int32_t v_x1_u32r;
	float H;

	v_x1_u32r = (t_fine - ((int32_t)76800));

	v_x1_u32r = (((((adc_H << 14) - (((int32_t)cal->dig_H4) << 20) -
			(((int32_t)cal->dig_H5) * v_x1_u32r)) +
		       ((int32_t)16384)) >>
		      15) *
		     (((((((v_x1_u32r * ((int32_t)cal->dig_H6)) >> 10) *
			  (((v_x1_u32r * ((int32_t)cal->dig_H3)) >> 11) +
			   ((int32_t)32768))) >>
			 10) +
			((int32_t)2097152)) *
			       ((int32_t)cal->dig_H2) +
		       8192) >>
		      14));

	v_x1_u32r =
		(v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) *
			       ((int32_t)cal->dig_H1)) >>
			      4));

	v_x1_u32r = (v_x1_u32r < 0) ? 0 : v_x1_u32r;
	v_x1_u32r = (v_x1_u32r > 419430400) ? 419430400 : v_x1_u32r;

	H = (v_x1_u32r >> 12);

	return H / 1024.0;
}

/**
 * @brief Calculate the altitue given an air pressure value
 * @note      Using "International Barometric Formula" Equation taken from BMP180 datasheet (page
 *16):
 *             http://www.adafruit.com/datasheets/BST-BMP180-DS000-09.pdf
 * @note      The equation on Wikipedia can give bad results at high altitude, reference:
 * http://forums.adafruit.com/viewtopic.php?f=22&t=58064
 *
 * @param[in]  pressure  The final pressure metric
 *
 * @return     Final altitude metric
 */
float bme280_get_altitude(float pressure)
{
	if (pressure <= (float)0)
		return 0;

	return 44330.0 *
	       (1.0 - pow(pressure / MEAN_SEA_LEVEL_PRESSURE, 0.190294957));
}

#if USE_WIRINGPI == 1
#ifndef NDEBUG
#pragma message "BME280 WiringPi"
#endif

char *bme280_wiringpi(void)
{
	int8_t fd = 0;
	int32_t t_fine = 0;

	// Variable should be retained for the whole execution
	static char out_string[DEFAULT_STRING_LEN] = "0";

	bme280_calibration_data cal;
	bme280_raw_data raw;
	bme280_metrics metrics;

	if ((fd = wiringPiI2CSetup(BME280_I2C_ADDRESS)) < 0) {
		print_error("wiringPiI2CSetup", 0,
			    "Failed to initialize BME280 I2C interface");

		return out_string;
	}

	bme280_get_calibration_data(fd, &cal);

	wiringPiI2CWriteReg8(fd, BME280_REGISTER_CONTROL_HUMID,
			     BME280_HUMIDITY_OVERSAMPLE1);
	wiringPiI2CWriteReg8(fd, BME280_REGISTER_CONTROL_MEAS,
			     BME280_PRESS_TEMP_OVERSAMPLE1);

	bme280_get_raw_data(fd, &raw);

	t_fine = bme280_calculate_compensation(&cal, raw.temperature);
	metrics.temperature = bme280_compensate_temp(t_fine);
	metrics.pressure =
		bme280_compensate_pressure(raw.pressure, &cal, t_fine) / 100;
	metrics.humidity =
		bme280_compensate_humidity(raw.humidity, &cal, t_fine);
	metrics.altitude = bme280_get_altitude(metrics.pressure);

	// JSON output
	snprintf(out_string, DEFAULT_STRING_LEN,
		 "{\"humidity\": \"%.2f\", \"temperature\": \"%.2f\","
		 "\"pressure\": \"%.2f\", \"altitude\": \"%.2f\"}",
		 metrics.humidity, metrics.temperature, metrics.pressure,
		 metrics.altitude);
	out_string[DEFAULT_STRING_LEN - 1] = '\0';

	wiringPiI2CWriteReg8(fd, BME280_REGISTER_CONTROL_MEAS,
			     BME280_SLEEP_MODE);
	close(fd);

	return out_string;
}

/**
 * @brief      Get calibration data from the BME280 sensor
 *
 * @param[in]  fd    File descriptor used to access the sensor
 * @param      data  Pointer to the data variable
 */
void bme280_get_calibration_data(int8_t fd, bme280_calibration_data *data)
{
	data->dig_T1 =
		(uint16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_T1);
	data->dig_T2 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_T2);
	data->dig_T3 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_T3);

	data->dig_P1 =
		(uint16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P1);
	data->dig_P2 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P2);
	data->dig_P3 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P3);
	data->dig_P4 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P4);
	data->dig_P5 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P5);
	data->dig_P6 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P6);
	data->dig_P7 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P7);
	data->dig_P8 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P8);
	data->dig_P9 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_P9);

	data->dig_H1 = (uint8_t)wiringPiI2CReadReg8(fd, BME280_REGISTER_DIG_H1);
	data->dig_H2 =
		(int16_t)wiringPiI2CReadReg16(fd, BME280_REGISTER_DIG_H2);
	data->dig_H3 = (uint8_t)wiringPiI2CReadReg8(fd, BME280_REGISTER_DIG_H3);
	data->dig_H4 =
		(wiringPiI2CReadReg8(fd, BME280_REGISTER_DIG_H4) << 4) |
		(wiringPiI2CReadReg8(fd, BME280_REGISTER_DIG_H4 + 1) & 0xF);
	data->dig_H5 =
		(wiringPiI2CReadReg8(fd, BME280_REGISTER_DIG_H5 + 1) << 4) |
		(wiringPiI2CReadReg8(fd, BME280_REGISTER_DIG_H5) >> 4);
	data->dig_H6 = (int8_t)wiringPiI2CReadReg8(fd, BME280_REGISTER_DIG_H6);
}

/**
 * @brief      Gather raw metrics from the BME280 sensor
 * @note      Address ranges: 0xF7 - 0xFC (temperature & pressure) 0xF7 -
 * 0xFE (temperature, pressure & humidity) Temp & pressure in unsigned 20-bit format Humidity in
 *unsigned 16-bit format
 *
 * @param[in]  fd    File descriptor used to access the sensor
 * @param      raw   Pointer to the raw variable
 *
 *             TODO Confirm this is a burst read
 */
void bme280_get_raw_data(int8_t fd, bme280_raw_data *raw)
{
	// To read registers, the address must first be sent in write mode
	wiringPiI2CWrite(fd, BME280_REGISTER_PRESSUREDATA);

	raw->pmsb = wiringPiI2CRead(fd);
	raw->plsb = wiringPiI2CRead(fd);
	raw->pxsb = wiringPiI2CRead(fd);

	raw->tmsb = wiringPiI2CRead(fd);
	raw->tlsb = wiringPiI2CRead(fd);
	raw->txsb = wiringPiI2CRead(fd);

	raw->hmsb = wiringPiI2CRead(fd);
	raw->hlsb = wiringPiI2CRead(fd);

	raw->temperature = 0;
	raw->temperature = (raw->temperature | raw->tmsb) << 8;
	raw->temperature = (raw->temperature | raw->tlsb) << 8;
	raw->temperature = (raw->temperature | raw->txsb) >> 4;

	raw->pressure = 0;
	raw->pressure = (raw->pressure | raw->pmsb) << 8;
	raw->pressure = (raw->pressure | raw->plsb) << 8;
	raw->pressure = (raw->pressure | raw->pxsb) >> 4;

	raw->humidity = 0;
	raw->humidity = (raw->humidity | raw->hmsb) << 8;
	raw->humidity = (raw->humidity | raw->hlsb);
}
#endif
