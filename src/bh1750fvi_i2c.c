#include "util.h"

#include "light/bh1750fvi_i2c.h"

/**
 * @brief      Gather light metric from BH1750FVI sensor
 *
 * @return     Metric as a string
 */
char *bh1750fvi_get_metric(void)
{
#if USE_WIRINGPI == 1

	return bh1750fvi_wiringpi();
#else

	return "0";
#endif
} // End bh1750fvi_get_metric

#if USE_WIRINGPI == 1
#ifndef NDEBUG
#pragma message "BH1750FVI WiringPi"
#endif

char *bh1750fvi_wiringpi(void)
{
	int8_t fd = 0;
	int16_t reading = 0;

	double lx_reading = 0;

	// Variable should be retained for the whole execution
	static char out_string[DEFAULT_STRING_LEN] = "0";

	if ((fd = wiringPiI2CSetup(BH1750FVI_I2C_ADDRESS)) < 0) {
		print_error("wiringPiI2CSetup", 0,
			    "Failed to initialize BH1750FVI I2C interface");

		return out_string;
	}

	wiringPiI2CWrite(fd, BH1750FVI_ONET_HIGH_RES2);
	function_delay((time_t)1, 0L);

	reading = wiringPiI2CReadReg16(fd, 0x00);
	lx_reading =
		(double)(((reading & 0xFF00) >> 8) | ((reading & 0x00FF) << 8));

	// On erroneous values, default to 0
	if ((lx_reading < 0) || (lx_reading > BH1750FVI_MAX_LX))
		lx_reading = 0;

	// sprintf(out_string, "%f", (lx_reading * 100) / BH1750FVI_MAX_LX);
	snprintf(out_string, DEFAULT_STRING_LEN, "%f", lx_reading);
	out_string[DEFAULT_STRING_LEN - 1] = '\0';

	return out_string;
} // End bh1750fvi_wiringpi
#endif
