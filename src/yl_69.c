#include <stdint.h>

#include "util.h"
#include "moisture/yl_69.h"

/**
 * @brief      Gather moisture content metric from the YL_69 sensor
 *
 * @note       This sensor relies on resistance to measure moisture content;
 * low value => lots of moisture, high value => little moisture
 *
 * @return     Metric as a string
 */
char *yl69_get_metric(void)
{
#if USE_WIRINGPI == 1

	return yl69_wiringpi();
#else

	return "0";
#endif
} // End yl69_get_metric

#if USE_WIRINGPI == 1
#ifndef NDEBUG
#pragma message "YL_69 WiringPi"
#endif

char *yl69_wiringpi(void)
{
	int16_t reading = 0;

	double normalized_reading = 0;

	// Variable lifetime = client execution
	static char out_string[DEFAULT_STRING_LEN] = "0";

	if (wiringPiSetup() < 0) {
		print_error("wiringPiSetup", 0,
			    "Failed to initialize YL_69 interface");

		return out_string;
	}

	// Power up sensor
	pinMode(YL_69_POWER_PIN, OUTPUT);
	digitalWrite(YL_69_POWER_PIN, HIGH);

// If using digital output
#if YL_69_MODE == 1
	pinMode(YL_69_DATA_PIN, INPUT);

	function_delay((time_t)1, 0L);

	reading = digitalRead(YL_69_DATA_PIN);
	normalized_reading = (double)reading == 0 ? 1 : 0;
#else // Analog read

	function_delay((time_t)1, 0L);

	if ((reading = mcp3008_get_adc_data(YL_69_ADC_CHANNEL)) ==
	    ERR_GENERAL_FAILURE) {
		print_error("yl96_get_metric", 0,
			    "Error occurred while querying the ADC");

		return out_string;
	}

	// Normalize to 0-100 range
	normalized_reading = (double)(YL_69_MAX_ANALOG_VALUE - reading) / 10;

	if (normalized_reading < (double)0)
		normalized_reading = 0;
#endif

	// Power down sensor
	digitalWrite(YL_69_POWER_PIN, LOW);

	snprintf(out_string, DEFAULT_STRING_LEN, "%f", normalized_reading);
	out_string[DEFAULT_STRING_LEN - 1] = '\0';

	return out_string;
} // End yl69_wiringpi
#endif

// Multiply by reference voltage then divide by input data upper limit
// voltage_in = (reading * 3.3) / 255;
