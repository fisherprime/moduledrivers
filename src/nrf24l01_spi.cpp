#include <cstring>

extern "C" {
#include "util.h"
}

#include "net/nrf24l01_spi.hpp"

RF24 gRadio(NRF24L01_CE_PIN, NRF24L01_CSN_PIN, NRF24L01_SPI_SPEED);
RF24Network gNetwork(gRadio);
RF24Mesh gMesh(gRadio, gNetwork);

void Nrf24l01Initialize(int NodeID)
{
	/**
	 * @note NodeID: 0 === Master node
	 */
	gMesh.setNodeID(NodeID);

	// Connect to the mesh
	gMesh.begin();

	gRadio.printDetails();
}

void Nrf24l01InitializeMaster(void)
{
	char *Data = (char *)calloc(DEFAULT_STRING_LEN, sizeof(char));

	gMesh.setNodeID(0);
	gMesh.begin();

	gRadio.printDetails();

	// Manage the mesh
	for (;;) {
		bzero(Data, DEFAULT_STRING_LEN);

		// Call mesh.update to keep the network updated
		gMesh.update();

		// Execute the DHCP service
		gMesh.DHCP();

		// Keep master alive, monitoring the network
		while (gNetwork.available()) {
			RF24NetworkHeader Header;
			gNetwork.peek(Header);

			switch (Header.type) {
			case 'M':
				gNetwork.read(Header, &Data,
					      DEFAULT_STRING_LEN);
				printf("[+] Received from node(%o): %s\n",
				       Header.from_node, Data);

				/**
					 * @brief Operation on valid data
					 *
					 * @brief Define in implementation
					 */
				nrf24_valid(Data);

				continue;
			default:
				gNetwork.read(Header, 0, 0);
				printf("Received bad type %d from node(%o)\n",
				       Header.from_node, Header.type);

				/**
					 * @brief Operation on invalid data
					 *
					 * @brief Define in implementation
					 */
				nrf24_invalid();

				break;
			}
		}
	}
}

int8_t Nrf24l01Communicate(char *Message)
{
	gMesh.update();

	/**
	 * @brief If a write fails, renew address
	 *
	 * @brief Attmepting upto 20 times with a 1s delay between attampt pairs
	 */
	for (int iter = 0; iter < RF24_RETRIES; ++iter) {
		if (gMesh.write(&Message, 'M', strlen(Message)))
			return SUCC_TERM;

		if (!gMesh.checkConnection()) {
			gMesh.renewAddress();

			if (gMesh.write(&Message, 'M', strlen(Message)))
				return SUCC_TERM;
		}

		function_delay((time_t)DELAY_SEC, DELAY_NANOSEC);
	}

	return ERR_GENERAL_FAILURE;
}

void nrf24_initialize(int node_id)
{
	Nrf24l01Initialize(node_id);
}

void nrf24_initialize_master(void)
{
	Nrf24l01InitializeMaster();
}

int8_t nrf24_communicate(char *message)
{
	return Nrf24l01Communicate(message);
}
