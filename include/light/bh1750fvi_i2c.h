/**
 * @file bh1750fvi_i2c.h
 * @brief  BH1750FVI Driver
 * @author fisherprime
 * @version 1.0.2
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _BH1750FVI_H
#define _BH1750FVI_H

// Necessary headers
// ////////////////////////////////
#include "util/i2c_wrapper.h"
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
// enum return_codes {
// };
// ////////////////////////////////

// Definitions
// ////////////////////////////////
#define BH1750FVI_I2C_ADDRESS 0x23

#define BH1750FVI_MAX_LX (double)65535.0

/**
 * Typical resolution times:
 *  120ms (high resolution 1 & 2), 16ms (low resolution) Resolutions:
 *  High_1: 1lx, High_2: 0.5lx, Low: 4lx
 */
// Continuous measurement
#define BH1750FVI_CONT_LOW_RES 0x13
#define BH1750FVI_CONT_HIGH_RES1 0x10
#define BH1750FVI_CONT_HIGH_RES2 0x11

// One-time measurement
#define BH1750FVI_ONET_HIGH_RES1 0x20
#define BH1750FVI_ONET_HIGH_RES2 0x21
#define BH1750FVI_ONET_LOW_RES 0x23

// ////////////////////////////////

// Function prototypes: BH1750FVI light reading module operations
// ////////////////////////////////
char *bh1750fvi_get_metric(void);

#if USE_WIRINGPI == 1
char *bh1750fvi_wiringpi(void);
#endif
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////

#endif // _BH1750FVI_H
