/**
 * @file i2c_wrapper.h
 * @brief  I2C wrapper functions
 * @author fisherprime
 * @version 1.0.2
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _I2C_WRAPPER_H
#define _I2C_WRAPPER_H

// Necessary headers
// ////////////////////////////////

#if USE_WIRINGPI == 1
// @headerfile provide I2C communication primitives
#include <wiringPiI2C.h>

#ifndef NDEBUG
#pragma message "Using wiringPi for I2C"
#endif
#elif USE_BCM2835 == 1
#include <bcm2835.h>

#ifndef NDEBUG
#pragma message "Using libbcm2835 for I2C"
#endif
#endif
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
// enum return_codes {
// };
// ////////////////////////////////

// Definitions
// ////////////////////////////////
// ////////////////////////////////

// Function prototypes: I2C wrapper operations
// ////////////////////////////////

/* int8_t wrap_i2c_begin(void);
 * int8_t wrap_i2c_end(void);
 *
 * int8_t wrap_i2c_write(const char *buf, size_t len);
 * int8_t wrap_i2c_read(const char *buf, size_t len);
 *
 * int8_t wrap_i2c_write_reg();
 * int8_t wrap_i2c_read_reg(); */
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////


#endif // _I2C_WRAPPER_H
