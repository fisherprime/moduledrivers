/**
 * @file gpio_wrapper.h
 * @brief  GPIO wrapper functions
 * @author fisherprime
 * @version 1.0.2
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _GPIO_WRAPPER_H
#define _GPIO_WRAPPER_H

// Necessary headers
// ////////////////////////////////

#if USE_WIRINGPI == 1
// @headerfile provide SPI communication primitives
#include <wiringPi.h>

#ifndef NDEBUG
#pragma message "Using wiringPi for GPIO"
#endif
#elif USE_BCM2835 == 1
#include <bcm2835.h>

#ifndef NDEBUG
#pragma message "Using libbcm2835 for GPIO"
#endif
#endif
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
// enum return_codes {
// };
// ////////////////////////////////

// Definitions
// ////////////////////////////////
// ////////////////////////////////

// Function prototypes: GPIO wrapper operations
// ////////////////////////////////
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////


#endif // _GPIO_WRAPPER_H
