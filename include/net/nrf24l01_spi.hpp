/**
 * @file mcp3008_spi.hpp
 * @brief  nRF24L01 SPI Driver
 * @author fisherprime
 * @version 1.0.2
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _NRF24L01_SPI_HPP
#define _NRF24L01_SPI_HPP

// Necessary headers
// ////////////////////////////////
// @headerfile common header file prologues "def/prologue.hpp"
 * @note using libbcm2835 for SPI, wiringPi on Aarch64 doesn't work, most likely my fault
 */
#include <RF24Mesh/RF24Mesh.h>
#include <RF24/RF24.h>
#include <RF24Network/RF24Network.h>
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
// enum return_codes {
// };
// ////////////////////////////////

// Definitions
// ////////////////////////////////

/**
 * @note When using libbcm2835, CSN is a pin number, while for wiringPi, CSN is the SPI channel
 */
#define NRF24L01_CE_PIN 22 // Chip enable, RPi B physical (15)
#define NRF24L01_CSN_PIN 1 // SPI chip select,  RPi B physical (26)
#define NRF24L01_SPI_SPEED 8000000 // 500,000 - 32,000,000

#define NRF24L01_TRANSMIT_ERROR "[!]" STR(ERR_GENERAL_FAILURE)

#define RF24_RETRIES 5
#define DELAY_SEC (time_t)0
#define DELAY_NANOSEC 500000L
// ////////////////////////////////

// Function prototypes(: <operation type>)
// ////////////////////////////////
void Nrf24l01Initialize(int NodeID);
void Nrf24l01InitializeMaster(void);
int8_t Nrf24l01Communicate(char *Message);

extern "C" {
// C wrapper functions
void nrf24_initialize(int NodeID);
void nrf24_initialize_master(void);
int8_t nrf24_communicate(char *Message);

// Declaration for functions defined in the implementation
extern void nrf24_valid(char *Data);
extern void nrf24_invalid();
}
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////

#endif // _NRF24L01_SPI_HPP
