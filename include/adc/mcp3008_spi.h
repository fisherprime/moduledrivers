/**
 * @file mcp3008_spi.h
 * @brief  MCP3008 SPI Driver
 * @author fisherprime
 * @version 1.0.2
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _MCP3008_SPI_H
#define _MCP3008_SPI_H

// Necessary headers
// ////////////////////////////////
#include "util/spi_wrapper.h"
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
// enum return_codes {
// };
// ////////////////////////////////

// Definitions
// ////////////////////////////////
#define ADC_CHANNEL_MIN 0
#define ADC_CHANNEL_MAX 7

#define MCP3008_SPI_CHANNEL 0
#define MCP3008_SPI_SPEED 1000000 // 500,000 - 32,000,000

/**
 * @brief In the single channel configuration, input voltages are compared to a common ground;
 * susceptible to noise
 *
 * @brief In the differential channel configuration, 2 input voltages within a common mode range are
 *compared
 */
#define MCP3008_SINGLE_CHANNEL_CONFIG 8
#define MCP3008_DIFFERENTIAL_CHANNEL_CONFIG 0
// ////////////////////////////////

// Function prototypes: MCP3008 ADC module operations
// ////////////////////////////////
int16_t mcp3008_get_adc_data(int analog_input_channel);

#if USE_WIRINGPI == 1
int16_t mcp3008_wiringpi(int analog_input_channel);
#endif
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////

#endif // _MCP3008_SPI_H
