/**
 * @file bme280_i2c.h
 * @brief      BME280 I2C Driver
 *
 * @note       Adapted from https://github.com/andreiva/raspberry-pi-bme280 and
 *https://github.com/adafruit/Adafruit_BME280_Library
 *
 * @note       Original compensation formulas from the BME280 datasheet
 *
 * @author     fisherprime
 * @version    1.0.2
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

/**
 * @note       Temperature unit: C
 * @note       Humidity unit: %
 * @note       Pressure unit: hPa
 */

#ifndef _BME280_I2C_H
#define _BME280_I2C_H

// Necessary headers
// ////////////////////////////////
#include "util/i2c_wrapper.h"
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
// enum return_codes {
// };
// ////////////////////////////////

// Definitions
// ////////////////////////////////
#define BME280_I2C_ADDRESS 0x77 // Alt address: 0x76

#define BME280_REGISTER_CAL26 0xE1
#define BME280_REGISTER_CHIPID 0xD0
#define BME280_REGISTER_CONFIG 0xF5
#define BME280_REGISTER_CONTROL_HUMID 0xF2
#define BME280_REGISTER_CONTROL_MEAS 0xF4
#define BME280_REGISTER_DIG_H1 0xA1
#define BME280_REGISTER_DIG_H2 0xE1
#define BME280_REGISTER_DIG_H3 0xE3
#define BME280_REGISTER_DIG_H4 0xE4
#define BME280_REGISTER_DIG_H5 0xE5
#define BME280_REGISTER_DIG_H6 0xE7
#define BME280_REGISTER_DIG_P1 0x8E
#define BME280_REGISTER_DIG_P2 0x90
#define BME280_REGISTER_DIG_P3 0x92
#define BME280_REGISTER_DIG_P4 0x94
#define BME280_REGISTER_DIG_P5 0x96
#define BME280_REGISTER_DIG_P6 0x98
#define BME280_REGISTER_DIG_P7 0x9A
#define BME280_REGISTER_DIG_P8 0x9C
#define BME280_REGISTER_DIG_P9 0x9E
#define BME280_REGISTER_DIG_T1 0x88
#define BME280_REGISTER_DIG_T2 0x8A
#define BME280_REGISTER_DIG_T3 0x8C
#define BME280_REGISTER_HUMIDDATA 0xFD
#define BME280_REGISTER_PRESSUREDATA 0xF7
#define BME280_REGISTER_SOFTRESET 0xE0
#define BME280_REGISTER_TEMPDATA 0xFA
#define BME280_REGISTER_VERSION 0xD1
#define BME280_RESET 0xB6

#define MEAN_SEA_LEVEL_PRESSURE (double)1013.25

/**
 * Refer to the BME280 datasheet for settings
 */
#define BME280_HUMIDITY_OVERSAMPLE1 0x01
#define BME280_PRESS_TEMP_OVERSAMPLE1 0x25
#define BME280_SLEEP_MODE 0x00
// ////////////////////////////////

// Typedefs
// ////////////////////////////////

/**
 * @brief Calibration data
 */
typedef struct {
	uint16_t dig_T1;
	int16_t dig_T2;
	int16_t dig_T3;

	uint16_t dig_P1;
	int16_t dig_P2;
	int16_t dig_P3;
	int16_t dig_P4;
	int16_t dig_P5;
	int16_t dig_P6;
	int16_t dig_P7;
	int16_t dig_P8;
	int16_t dig_P9;

	uint8_t dig_H1;
	int16_t dig_H2;
	uint8_t dig_H3;
	int16_t dig_H4;
	int16_t dig_H5;
	int8_t dig_H6;
} bme280_calibration_data;

/**
 * @brief Raw sensor measurements
 */
typedef struct {
	uint8_t pmsb;
	uint8_t plsb;
	uint8_t pxsb;

	uint8_t tmsb;
	uint8_t tlsb;
	uint8_t txsb;

	uint8_t hmsb;
	uint8_t hlsb;

	uint32_t temperature;
	uint32_t pressure;
	uint32_t humidity;
} bme280_raw_data;

/**
 * @brief Sanitized sensor data
 */
typedef struct {
	float temperature;
	float pressure;
	float humidity;
	float altitude;
} bme280_metrics;
// ////////////////////////////////

// Function prototypes: BME280 sensing module operations
// ////////////////////////////////
void bme280_get_calibration_data(int8_t fd, bme280_calibration_data *cal);
void bme280_get_raw_data(int8_t fd, bme280_raw_data *raw);

char *bme280_get_metrics(void);
float bme280_get_altitude(float pressure);

int32_t bme280_calculate_compensation(bme280_calibration_data *cal,
				      int32_t adc_T);
float bme280_compensate_temp(int32_t t_fine);
float bme280_compensate_pressure(int32_t adc_P, bme280_calibration_data *cal,
				 int32_t t_fine);
float bme280_compensate_humidity(int32_t adc_H, bme280_calibration_data *cal,
				 int32_t t_fine);

#if USE_WIRINGPI == 1
char *bme280_wiringpi(void);
#endif
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////

#endif // _BME280_I2C_H
