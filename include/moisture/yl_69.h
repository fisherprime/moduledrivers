/**
 * @file yl_69.h
 * @brief  YL_69 Driver
 * @author fisherprime
 * @version 1.0.2
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _YL_69_H
#define _YL_69_H

// Necessary headers
#include "util/gpio_wrapper.h"

// @headerfile Analog-Digital Converter driver
#include "adc/mcp3008_spi.h"
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
// enum return_codes {
// };
// ////////////////////////////////

// Definitions
// ////////////////////////////////
// Not powering with VCC to prolong sensor's lifespan
#define YL_69_ADC_CHANNEL 0
#define YL_69_DATA_PIN 2
#define YL_69_POWER_PIN 7
#define YL_69_MODE 0 // 0 (analog), 1 (digital)

// Doesn't rely on potentiometer value, rather the sensor's corrosion, ...
#define YL_69_MAX_ANALOG_VALUE 1000
// ////////////////////////////////

// Function prototypes: YL_69 moisture reading module operations
// ////////////////////////////////A
char *yl69_get_metric(void);

#if USE_WIRINGPI == 1
char *yl69_wiringpi(void);
#endif
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////

#endif // _YL_69_H
