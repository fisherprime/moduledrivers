# Setup

## Interface generation

Preferring swig over manual creation **cgo can't use CXX code due to its
name mangling, a C wrapper is thus necessary**

Using intger size 32, should be adjusted based on system

Interface build command:

```sh
cd src
ln nrf24l01_spi.cpp interface/go/RF24Comm
cd interface/go/RF24Comm
swig -c++ -go -cgo -intgosize 32 -module RF24Comm nrf24l01_spi.cpp
```

# Building

## Using wiringPi

Defaults to wiringPI, defined by "cmake.definitions["USE_WIRINGPI"] = 1" in
conanfile.py

No implementations for direct SPI, I2c, ... module manipulations exist

## Update git submodules

```sh
git submodule update init --recursive
```

## Configure external dependencies

c_headers

```sh
cd external/C
conan install .; conan package .
cd ../..
```

RF24

```sh
cd external/RF24
configure
cd ../..
```

## The drivers

```sh
conan install .; conan package .
```

# Implementation

The net/nrf24l01_spi header contains declarations for the following 2
functions that need to be defined in the implementation for use

```c
// Declaration for functions defined in the implementation
extern void nrf24_valid(char *Data);
extern void nrf24_invalid();
```
