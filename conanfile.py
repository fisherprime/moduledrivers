"""
File: conanfile.py
Author: fisherprime
Email: N/A
Gitlab: https://gitlab.com/fisherprime
Description: Conan install script
"""

from multiprocessing import cpu_count

from conans import CMake, ConanFile, tools


class HeadersConan(ConanFile):
    #  version = "1.x"
    description = "Module drivers for wiringPi"
    license = "MIT"
    name = "module_drivers"
    url = "https://gitlab.com/fisherprime/moduledrivers"

    build_policy = "missing"  # always
    generators = "cmake"
    #  requires = ("flatbuffers/[~=1.11.0]@google/stable",
                #  "doctest/2.3.1@bincrafters/stable")
    requires = ("flatbuffers/[~=2.0.5]",
                "doctest/2.4.8")
    settings = "os", "compiler", "build_type", "arch"

    #  def configure(self):
    #  # Sample cross-building architecture setting
    #  self.settings.arch = "armv8"

    def _configure_cmake(self):
        cmake = CMake(self, generator="Ninja", build_type="Release")
        cmake.definitions["USE_FLATCC"] = 1
        cmake.definitions["USE_WIRINGPI"] = 1

        cmake.configure()

        return cmake

    def _test(self, cmake):
        if cmake.generator is not "Ninja":
            cmake.test()

            return

    def build(self):
        cmake = self._configure_cmake()

        cmake.build()
        self._test(cmake)

        return cmake

    def package(self):
        cmake = self._configure_cmake()

        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["lib" + self.name + ".a"]
